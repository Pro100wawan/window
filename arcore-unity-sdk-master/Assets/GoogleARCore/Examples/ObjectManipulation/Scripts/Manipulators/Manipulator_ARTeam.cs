﻿using Game.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;
using static NativeGallery;

public enum TypeLoadContent
{
    None,
    image,
    video,
    video2,
}

[Serializable]
public class PrepareJson
{
    public TypeLoadContent namePrepare;
    public int number;
    public bool isPrepare;
}

public class Manipulator_ARTeam : MonoBehaviour
{

    public static bool IsSettings = false;
    public static int numberVideo = 0;

    public GameObject panelSetings;


    public Toggle toggle;
    public Slider sliderScale;
    public Slider sliderPosition;
    public Text textURL;
    public GameObject btnNextInfo;
    public Image likeBtn;
    public Image likeBtnRed;
    public Sprite pauseSprite;
    public Sprite nextSprite;

    private bool isChangeText = false;
    private bool isPause;
    private float timerDefault;
    private float lastRefrech;

    public InputField scale;
    public InputField x;
    public InputField y;
    public InputField z;

    public Text helpText1big;
    public Text helpText1small;
    public Text textScan;
    public Text helpNextBtn;

    public Button buttonNext;
    public Button likeButton;
    public GameObject helpNext;


    public VideoPlayerLoad videoPlayer;
    public VideoPlayerLoad videoPlayer2;

    public RawImage top;
    public RawImage bottom;
    public RawImage center;
    public Image timerImage;

    public GameObject objecrManipulation;
    public static Texture textureImage;
    public PrepareJson[] prepareJsons;


    public Text nameText;
    public Text cityText;


    public bool GetIsPause()
    {
        return isPause;
    }

    // Start is called before the first frame update
    void Start()
    {
        FindObjectManipulation();
        LoadParametrs();

        prepareJsons = new PrepareJson[Preloader.window.WindowList.Count];

        for (int i = 0; i < prepareJsons.Length; i++)
        {
            prepareJsons[i] = new PrepareJson();

            prepareJsons[i].number = i;
        }

        InitJson();

        isPause = false;
        helpText1big.text = Preloader.window.HelpText1big;
        helpText1small.text = Preloader.window.HelpText1small;
        helpNextBtn.text = Preloader.window.HelpNextBtn;
        textScan.text = "1/3";
        timerDefault = Preloader.window.TimerSec;
        lastRefrech = Time.time;
        timerImage.fillAmount = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsSettings)
            return;

        processDragPosition();
        processDragScale();



    }


    private void InitJson()
    {
        bool isFirstVideo = false;
        bool isSecondVideo = false;
        bool isTextureimage = false;


        if (Preloader.window.WindowList.Count == 1)
        {
            if (Preloader.window.WindowList[0].type == "video")
            {
                prepareJsons[0].isPrepare = true;
                prepareJsons[0].namePrepare = TypeLoadContent.video;

                videoPlayer.videoPlayer.url = Preloader.window.WindowList[0].path;
                videoPlayer.PrepareVideo();
            }
            else
            {
                bool isPrepareTExture = false;
                prepareJsons[0].isPrepare = true;
                prepareJsons[0].namePrepare = TypeLoadContent.image;

                StartCoroutine(GetTexture(Preloader.window.WindowList[0].path, value => textureImage = value, value => isPrepareTExture = value));
            }
        }
        else if (Preloader.window.WindowList.Count > 1)
        {
            int i = 0;

            foreach (var window in Preloader.window.WindowList)
            {
                if (window.type == "image" && !isTextureimage)
                {
                    bool isPrepareTExture = false;

                    StartCoroutine(GetTexture(window.path, value => textureImage = value, value => isPrepareTExture = value));
                    isTextureimage = true;

                    prepareJsons[i].isPrepare = true;
                    prepareJsons[i].namePrepare = TypeLoadContent.image;

                    i++;
                    continue;
                }

                if (window.type == "video" && !isFirstVideo)
                {
                    videoPlayer.videoPlayer.url = window.path;
                    videoPlayer.PrepareVideo();
                    isFirstVideo = true;

                    prepareJsons[i].isPrepare = true;
                    prepareJsons[i].namePrepare = TypeLoadContent.video;

                    i++;
                    continue;
                }

                if (window.type == "video" && !isSecondVideo)
                {
                    videoPlayer2.videoPlayer.url = window.path;
                    videoPlayer2.PrepareVideo();
                    isSecondVideo = true;

                    prepareJsons[i].isPrepare = true;
                    prepareJsons[i].namePrepare = TypeLoadContent.video2;

                    i++;
                    continue;
                }

                i++;

            }
        }
    }

    public static IEnumerator GetTexture(string pathImage, System.Action<Texture> texturePrepare, System.Action<bool> isPrepare)
    {
        textureImage = null;

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(pathImage);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            isPrepare(true);

            Texture texture = ((DownloadHandlerTexture)www.downloadHandler).texture;

            texturePrepare(texture);
        }
    }

    public void SetingsON()
    {
        if (objecrManipulation == null) FindObjectManipulation();
        if (panelSetings.activeSelf) { panelSetings.SetActive(false); IsSettings = false; }
        else
        {

            panelSetings.SetActive(true); IsSettings = true;


            UpdateParametrsScale();
        }

        SaveParametrs();
    }


    private void UpdateParametrsScale()
    {
        scale.text = objecrManipulation.transform.localScale.x.ToString("0.000");
        x.text = objecrManipulation.transform.localPosition.x.ToString("0.000");
        y.text = objecrManipulation.transform.localPosition.y.ToString("0.000");
        z.text = objecrManipulation.transform.localPosition.z.ToString("0.000");
        isChangeText = true;

    }

    public void UpdateParametrObject()
    {
        if (isChangeText)
        {
            float scaleParam = float.Parse(scale.text);
            objecrManipulation.transform.localScale = new Vector3(scaleParam, scaleParam, scaleParam);
        }
    }
    public void UpdateParametrObjectX()
    {
        if (isChangeText)
        {

            objecrManipulation.transform.localPosition = new Vector3(float.Parse(x.text), objecrManipulation.transform.localPosition.y, objecrManipulation.transform.localPosition.z);
        }
    }
    public void UpdateParametrObjectY()
    {
        if (isChangeText)
        {

            objecrManipulation.transform.localPosition = new Vector3(objecrManipulation.transform.localPosition.x, float.Parse(y.text), objecrManipulation.transform.localPosition.z);
        }
    }
    public void UpdateParametrObjectZ()
    {
        if (isChangeText)
        {

            objecrManipulation.transform.localPosition = new Vector3(objecrManipulation.transform.localPosition.x, objecrManipulation.transform.localPosition.y, float.Parse(z.text));
        }
    }

    public void ScaleOnValueChanget()
    {
        if (objecrManipulation == null)
            return;

        //objecrManipulation.transform.localScale = new Vector3(sliderScale.value, sliderScale.value, sliderScale.value);

    }

    public void PositionOnValueChanget()
    {

        //  objecrManipulation.transform.localPosition = new Vector3(0f, 0f, sliderPosition.value * -2f);

    }

    public void URLOnValueChanget()
    {
        if (objecrManipulation == null)
            return;

        VideoPlayer videoPlayer = objecrManipulation.GetComponent<VideoPlayer>();
        if (videoPlayer.isPlaying)
        {
            videoPlayer.Stop();
        }

        videoPlayer.url = textURL.text;
        videoPlayer.Play();

    }

    private int isFirst = 0;


    public void OnLikeVideo()
    {
        if (likeBtnRed.gameObject.activeSelf)
            return;

        likeBtnRed.gameObject.SetActive(true);

        StartCoroutine(LikeVideo());
    }

    private IEnumerator LikeVideo()
    {
        //WWWForm form = new WWWForm();
        //form.AddField("id", Preloader.window.WindowList[numberVideo].uniqid.ToString());
        //form.AddField("phone", SystemInfo.deviceUniqueIdentifier);

        //WWW www = new WWW(xmlManager.xmlSet.Info.UrlQuestion + user + "&q=" + id + "&a=" + answerId + "&c=" + comment);
        //yield return www;
        UnityWebRequest www = UnityWebRequest.Get("http://lomw.ar.team/api/v2/like.php?id=" + Preloader.window.WindowList[numberVideo].uniqid.ToString() + "&phone=" + SystemInfo.deviceUniqueIdentifier);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("Server error: " + www.error);
        }
        else
        {
            Debug.Log("Certificate: " + www.downloadHandler.text);
        }
    }

    public void NextUrl()
    {
        switch (prepareJsons[numberVideo].namePrepare)
        {
            default:
            case TypeLoadContent.None:
                break;
            case TypeLoadContent.image:
                prepareJsons[numberVideo].isPrepare = false;
                prepareJsons[numberVideo].namePrepare = TypeLoadContent.None;
                bool isFound = false;

                for (int i = numberVideo + 1; i < Preloader.window.WindowList.Count; i++)
                {
                    if (Preloader.window.WindowList[i].type == "image")
                    {
                        prepareJsons[i].isPrepare = true;
                        prepareJsons[i].namePrepare = TypeLoadContent.image;

                        bool isPrepareTExture = false;
                        StartCoroutine(GetTexture(Preloader.window.WindowList[i].path, value => textureImage = value, value => isPrepareTExture = value));

                        isFound = true;
                        break;
                    }
                }

                if (!isFound)
                {
                    for (int i = 0; i < Preloader.window.WindowList.Count; i++)
                    {
                        if (Preloader.window.WindowList[i].type == "image")
                        {
                            prepareJsons[i].isPrepare = true;
                            prepareJsons[i].namePrepare = TypeLoadContent.image;

                            if (i != numberVideo)
                            {
                                bool isPrepareTExture = false;
                                StartCoroutine(GetTexture(Preloader.window.WindowList[i].path, value => textureImage = value, value => isPrepareTExture = value));
                            }


                            isFound = true;
                            break;
                        }
                    }
                }
                break;
            case TypeLoadContent.video:
                prepareJsons[numberVideo].isPrepare = false;
                prepareJsons[numberVideo].namePrepare = TypeLoadContent.None;
                bool isFoundVideo = false;

                for (int i = numberVideo + 1; i < Preloader.window.WindowList.Count; i++)
                {
                    if (Preloader.window.WindowList[i].type == "video")
                    {
                        if (prepareJsons[i].namePrepare == TypeLoadContent.None)
                        {
                            prepareJsons[i].isPrepare = true;
                            prepareJsons[i].namePrepare = TypeLoadContent.video;
                            videoPlayer.videoPlayer.Stop();

                            videoPlayer.videoPlayer.url = Preloader.window.WindowList[i].path;
                            videoPlayer.PrepareVideo();

                            isFoundVideo = true;
                            break;

                        }
                    }
                }


                if (!isFoundVideo)
                {
                    for (int i = 0; i < Preloader.window.WindowList.Count; i++)
                    {
                        if (Preloader.window.WindowList[i].type == "video")
                        {
                            if (prepareJsons[i].namePrepare == TypeLoadContent.None)
                            {
                                prepareJsons[i].isPrepare = true;
                                prepareJsons[i].namePrepare = TypeLoadContent.video;
                                videoPlayer.videoPlayer.Stop();

                                if (i != numberVideo)
                                {

                                    videoPlayer.videoPlayer.url = Preloader.window.WindowList[i].path;
                                    videoPlayer.PrepareVideo();
                                }

                                isFoundVideo = true;
                                break;
                            }
                        }
                    }
                }

                break;
            case TypeLoadContent.video2:

                prepareJsons[numberVideo].isPrepare = false;
                prepareJsons[numberVideo].namePrepare = TypeLoadContent.None;
                bool isFoundVideo2 = false;

                for (int i = numberVideo + 1; i < Preloader.window.WindowList.Count; i++)
                {
                    if (Preloader.window.WindowList[i].type == "video")
                    {
                        if (prepareJsons[i].namePrepare == TypeLoadContent.None)
                        {
                            prepareJsons[i].isPrepare = true;
                            prepareJsons[i].namePrepare = TypeLoadContent.video2;
                            videoPlayer2.videoPlayer.Stop();

                            videoPlayer2.videoPlayer.url = Preloader.window.WindowList[i].path;
                            videoPlayer2.PrepareVideo();

                            isFoundVideo2 = true;
                            break;

                        }
                    }
                }


                if (!isFoundVideo2)
                {
                    for (int i = 0; i < Preloader.window.WindowList.Count; i++)
                    {
                        if (Preloader.window.WindowList[i].type == "video")
                        {
                            if (prepareJsons[i].namePrepare == TypeLoadContent.None)
                            {
                                prepareJsons[i].isPrepare = true;
                                prepareJsons[i].namePrepare = TypeLoadContent.video2;
                                videoPlayer2.videoPlayer.Stop();

                                if (i != numberVideo)
                                {
                                    videoPlayer2.videoPlayer.url = Preloader.window.WindowList[i].path;
                                    videoPlayer2.PrepareVideo();
                                }

                                isFoundVideo2 = true;
                                break;
                            }
                        }
                    }
                }
                break;
        }

        numberVideo++;
        if (numberVideo > Preloader.window.WindowList.Count - 1)
            numberVideo = 0;

        lastRefrech = Time.time;
       // buttonNext.interactable = false;
        timerImage.fillAmount = 1f;
        FunctionUpdater.Create(()=> 
        {
            float timer = Time.time - lastRefrech;
            if (timer > timerDefault)
            {
                if (isPause)
                {
                    lastRefrech = Time.time;
                    timerImage.fillAmount = 1;

                    return false;
                }

               // buttonNext.interactable = true;
                NextUrl();
                timerImage.fillAmount = 0f;
                return true;
            }

            timerImage.fillAmount = 1 - timer / timerDefault;

            return false;
        });




        likeBtnRed.gameObject.SetActive(false);
        top.GetComponent<AspectRatioFitter>().enabled = false;
        bottom.GetComponent<AspectRatioFitter>().enabled = false;
        center.GetComponent<AspectRatioFitter>().enabled = false;

        top.rectTransform.anchoredPosition = new Vector2(top.rectTransform.anchoredPosition.x, 2);
        bottom.rectTransform.anchoredPosition = new Vector2(bottom.rectTransform.anchoredPosition.x, -2);

        top.rectTransform.sizeDelta = new Vector2(3, 2);
        bottom.rectTransform.sizeDelta = new Vector2(3, 2);
        center.rectTransform.sizeDelta = new Vector2(3, 2);

        switch (prepareJsons[numberVideo].namePrepare)
        {
            default:
            case TypeLoadContent.None:
                break;
            case TypeLoadContent.image:

                Game.Utils.FunctionUpdater.Create(() =>
                {
                    if (textureImage != null)
                    {


                        top.texture = textureImage;
                        bottom.texture = textureImage;
                        center.texture = textureImage;

                        float aspectRatio = (float)textureImage.width / (float)textureImage.height;

                        top.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                        top.GetComponent<AspectRatioFitter>().enabled = true;
                        bottom.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                        bottom.GetComponent<AspectRatioFitter>().enabled = true;
                        center.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                        center.GetComponent<AspectRatioFitter>().enabled = true;

                        top.rectTransform.anchoredPosition = new Vector2(top.rectTransform.anchoredPosition.x, top.rectTransform.sizeDelta.y);
                        bottom.rectTransform.anchoredPosition = new Vector2(bottom.rectTransform.anchoredPosition.x,-bottom.rectTransform.sizeDelta.y);

                        return true;
                    }

                    return false;
                });

                break;
            case TypeLoadContent.video:
                Game.Utils.FunctionUpdater.Create(() =>
                {
                    if (videoPlayer.videoPlayer.isPrepared)
                    {

                        videoPlayer.videoPlayer.Play();

                        videoPlayer.videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                        //Assign the Audio from Video to AudioSource to be played
                        videoPlayer.videoPlayer.EnableAudioTrack(0, true);
                        videoPlayer.videoPlayer.SetTargetAudioSource(0, videoPlayer.videoPlayer.GetComponent<AudioSource>());

                        top.texture = videoPlayer.videoPlayer.targetTexture;
                        bottom.texture = videoPlayer.videoPlayer.targetTexture;
                        center.texture = videoPlayer.videoPlayer.targetTexture;

                        return true;
                    }

                    return false;
                });

                break;
            case TypeLoadContent.video2:

                Game.Utils.FunctionUpdater.Create(() =>
                {
                    if (videoPlayer2.videoPlayer.isPrepared)
                    {

                        videoPlayer2.videoPlayer.Play();

                        videoPlayer2.videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                        //Assign the Audio from Video to AudioSource to be played
                        videoPlayer2.videoPlayer.EnableAudioTrack(0, true);
                        videoPlayer2.videoPlayer.SetTargetAudioSource(0, videoPlayer.videoPlayer.GetComponent<AudioSource>());

                        top.texture = videoPlayer2.videoPlayer.targetTexture;
                        bottom.texture = videoPlayer2.videoPlayer.targetTexture;
                        center.texture = videoPlayer2.videoPlayer.targetTexture;

                        return true;
                    }

                    return false;
                });
                break;

        }

        nameText.text = Preloader.window.WindowList[numberVideo].name;
        cityText.text = Preloader.window.WindowList[numberVideo].city;

    }

    public void URLChange()
    {
        if (objecrManipulation == null) FindObjectManipulation();

        if (isFirst == 0)
        {
            videoPlayer.videoPlayer.Stop();
            isFirst = 1;

            numberVideo++;
            if (numberVideo > Preloader.window.WindowList.Count - 1)
                numberVideo = 0;


            videoPlayer.videoPlayer.url = Preloader.window.WindowList[numberVideo].path;
            videoPlayer.PrepareVideo();


            Game.Utils.FunctionUpdater.Create(() =>
            {
                bool isPrepare = false;

                if (videoPlayer2.videoPlayer.isPaused || videoPlayer2.videoPlayer.isPrepared)
                {
                    videoPlayer2.videoPlayer.Play();


                    videoPlayer2.videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                    //Assign the Audio from Video to AudioSource to be played
                    videoPlayer2.videoPlayer.EnableAudioTrack(0, true);
                    videoPlayer2.videoPlayer.SetTargetAudioSource(0, videoPlayer2.videoPlayer.GetComponent<AudioSource>());

                    top.material.mainTexture = videoPlayer2.videoPlayer.targetTexture;
                    bottom.material.mainTexture = videoPlayer2.videoPlayer.targetTexture;
                    center.material.mainTexture = videoPlayer2.videoPlayer.targetTexture;

                    return true;
                }
                else if (!isPrepare)
                {
                    videoPlayer2.videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                    //Assign the Audio from Video to AudioSource to be played
                    videoPlayer2.videoPlayer.EnableAudioTrack(0, true);
                    videoPlayer2.videoPlayer.SetTargetAudioSource(0, videoPlayer2.videoPlayer.GetComponent<AudioSource>());

                    videoPlayer2.videoPlayer.Prepare();

                    isPrepare = true;
                }

                return false;
            });



        }
        else if (isFirst == 1)
        {
            videoPlayer2.videoPlayer.Stop();
            isFirst = 0;

            numberVideo++;
            if (numberVideo > Preloader.window.WindowList.Count - 1)
                numberVideo = 0;


            videoPlayer2.videoPlayer.url = Preloader.window.WindowList[numberVideo].path;
            videoPlayer2.PrepareVideo();

            Game.Utils.FunctionUpdater.Create(() =>
            {
                bool isPrepare = false;

                if (videoPlayer.videoPlayer.isPaused || videoPlayer.videoPlayer.isPrepared)
                {
                    videoPlayer.videoPlayer.Play();

                    videoPlayer.videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                    //Assign the Audio from Video to AudioSource to be played
                    videoPlayer.videoPlayer.EnableAudioTrack(0, true);
                    videoPlayer.videoPlayer.SetTargetAudioSource(0, videoPlayer.videoPlayer.GetComponent<AudioSource>());

                    top.material.mainTexture = videoPlayer.videoPlayer.targetTexture;
                    bottom.material.mainTexture = videoPlayer.videoPlayer.targetTexture;
                    center.material.mainTexture = videoPlayer.videoPlayer.targetTexture;

                    return true;
                }
                else if (!isPrepare)
                {
                    videoPlayer.videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                    //Assign the Audio from Video to AudioSource to be played
                    videoPlayer.videoPlayer.EnableAudioTrack(0, true);
                    videoPlayer.videoPlayer.SetTargetAudioSource(0, videoPlayer.videoPlayer.GetComponent<AudioSource>());

                    videoPlayer.videoPlayer.Prepare();

                    isPrepare = true;
                }

                return false;
            });
        }

    }

    public void UrlGaleryOpen()
    {


        NativeGallery.Permission permission = NativeGallery.GetVideoFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {
                VideoPlayer videoPlayer = objecrManipulation.GetComponent<VideoPlayer>();
                // Play the selected video
                videoPlayer.Stop();
                videoPlayer.url = path;
                videoPlayer.Play();
            }
        }, "Select a video");

        Debug.Log("Permission result: " + permission);


    }

    void SaveParametrs()
    {
        PlayerPrefs.SetFloat("Slider Scale", sliderScale.value);
        PlayerPrefs.SetFloat("Slider Position", sliderPosition.value);
        PlayerPrefs.SetString("URL", textURL.text);
    }

    void LoadParametrs()
    {
        PlayerPrefs.GetFloat("Slider Scale");
        PlayerPrefs.GetFloat("Slider Position");
        PlayerPrefs.GetString("URL");
    }

    void FindObjectManipulation()
    {
        if (objecrManipulation == null)
        {
            objecrManipulation = GameObject.FindGameObjectWithTag("ARObjectManipulation");

        }

    }

    public float scaleSpeed = 0.05f;
    private float minSize = 0.001f;
    private float maxSize = 5f;
    private Vector3 screenPoint;
    private Vector3 offset;
    private bool isClickDrag;

    private void processDragScale()
    {
        FindObjectManipulation();


        if (Input.touchCount >= 2)
        {
            isChangeText = false;

            //Store input
            Touch fing = Input.GetTouch(0);
            Touch fingTwo = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = fing.position - fing.deltaPosition; //previous frame position
            Vector2 touchOnePrevPos = fingTwo.position - fingTwo.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude; //the length of the path between each tach
            float touchDeltaMag = (fing.position - fingTwo.position).magnitude;

            float deltaMagnitudeDiff = (prevTouchDeltaMag - touchDeltaMag) * -1.0f; //length of pinch

            // Camera.main.fieldOfView += deltaMagnitudeDiff * ZoomSpeed; //change the "scale" of the camera

            objecrManipulation.transform.localScale += Vector3.one * deltaMagnitudeDiff * scaleSpeed * Time.deltaTime;

            objecrManipulation.transform.localScale = new Vector3(Mathf.Clamp(objecrManipulation.transform.localScale.x, minSize, maxSize), Mathf.Clamp(objecrManipulation.transform.localScale.y, minSize, maxSize), Mathf.Clamp(objecrManipulation.transform.localScale.z, minSize, maxSize)); //range limiting

            UpdateParametrsScale();
        }

    }


    private void processDragMove()
    {
        FindObjectManipulation();

        if (Input.GetMouseButtonDown(0))
        {
            isChangeText = false;
            screenPoint = Camera.main.WorldToScreenPoint(objecrManipulation.transform.position);
            offset = objecrManipulation.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
            isClickDrag = true;
        }
        else if (Input.GetMouseButton(0))
        {
            if (!isClickDrag)
                return;

            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            print(Camera.main.ScreenToWorldPoint(curScreenPoint).ToString("00.000") + " " + offset);

            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            Vector3 positionMove;


            //if (toggle.isOn)
            //    positionMove = curPosition;
            //else
            //    positionMove = new Vector3(objecrManipulation.transform.position.x, curPosition.x, curPosition.z);

            objecrManipulation.transform.position = curPosition;

        }

    }

    public float postionDragSpeed = 0.01f;
    private Vector3 currentPosition;
    private Vector3 deltaPositon;
    private Vector3 lastPositon;
    public Vector3 desiredPosition;
    private Vector2 fingMove;

    private void processDragPosition()
    {

        if (Input.GetMouseButtonDown(0))
        {
            lastPositon = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            currentPosition = Input.mousePosition;
            deltaPositon = currentPosition - lastPositon;

            fingMove = deltaPositon;


            lastPositon = currentPosition;

            if (toggle.isOn)
                desiredPosition = new Vector3(fingMove.x * postionDragSpeed, fingMove.y * postionDragSpeed, 0);
            else
                desiredPosition = new Vector3(fingMove.x * postionDragSpeed, 0, fingMove.y * postionDragSpeed * (-1f));


            objecrManipulation.transform.localPosition += desiredPosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            desiredPosition = transform.localPosition;
            UpdateParametrsScale();
        }
    }

    public void RestartScene()
    {
        Destroy(Preloader.Instanse.gameObject);
        SceneManager.LoadScene("preloader");
    }


    public void OnPausePlayVideo()
    {
        Image imageBtn = buttonNext.targetGraphic.GetComponent<Image>();

        if (imageBtn.sprite.name == nextSprite.name)
        {
            buttonNext.targetGraphic.GetComponent<Image>().sprite = pauseSprite;
            isPause = false;
        }
        else
        {
            buttonNext.targetGraphic.GetComponent<Image>().sprite = nextSprite;
            isPause = true;
        }

      
    }
}
