﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CubeTexture_ARTeam : MonoBehaviour
{

    private VideoPlayer videoPlayer;

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
    }

    private void Start()
    {

        videoPlayer.started += VideoPlayer_started;


       
    }

    private void VideoPlayer_started(VideoPlayer source)
    {
        foreach (MeshRenderer child in GetComponentsInChildren<MeshRenderer>())
        {
            child.material.mainTexture = videoPlayer.targetTexture;
        }
    }
}
