//-----------------------------------------------------------------------
// <copyright file="PawnManipulator.cs" company="Google">
//
// Copyright 2019 Google LLC. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

namespace GoogleARCore.Examples.ObjectManipulation
{
    using Game.Utils;
    using GoogleARCore;
    using GoogleARCore.Examples.Common;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    /// Controls the placement of objects via a tap gesture.
    /// </summary>
    public class PawnManipulator : Manipulator
    {
        /// <summary>
        /// The first-person camera being used to render the passthrough camera image (i.e. AR
        /// background).
        /// </summary>
        public Camera FirstPersonCamera;

        /// <summary>
        /// A prefab to place when a raycast from a user touch hits a plane.
        /// </summary>
        public GameObject PawnPrefab;

        /// <summary>
        /// Manipulator prefab to attach placed objects to.
        /// </summary>
        public GameObject ManipulatorPrefab;

        public Manipulator_ARTeam videoPlayerLoad;

        public GameObject planeGenerator;

        public GameObject pointCloud;
        public GameObject stayImageHelp;

        private GameObject pawnObject;
        private GameObject manipulator;
        private bool isHit = false;
        private Anchor anchor;

        private bool isVertocal;
        private bool isHorisontal;
        private float lastRefrech;


        /// <summary>
        /// Returns true if the manipulation can be started for the given gesture.
        /// </summary>
        /// <param name="gesture">The current gesture.</param>
        /// <returns>True if the manipulation can be started.</returns>
        protected override bool CanStartManipulationForGesture(TapGesture gesture)
        {
            if (gesture.TargetObject == null)
            {
                return true;
            }

            return false;
        }

        protected override void Update()
        {
            base.Update();

            // Check that motion tracking is tracking.
            if (Session.Status != SessionStatus.Tracking || isHit)
            {
                return;
            }

            if (isVertocal && isHorisontal)
            {
                videoPlayerLoad.textScan.text = "3/3";
                videoPlayerLoad.helpText1big.text = Preloader.window.HelpText3big;
                videoPlayerLoad.helpText1small.text = Preloader.window.HelpText3small;
                stayImageHelp.SetActive(true);

                if (Preloader.window.HelpStayBtn != null && Preloader.window.HelpStayBtn != "")
                {
                    stayImageHelp.GetComponentInChildren<Text>().text = Preloader.window.HelpStayBtn;
                }

            }
            else if(isHorisontal && !isVertocal) 
            {
                videoPlayerLoad.textScan.text = "2/3";
                videoPlayerLoad.helpText1big.text = Preloader.window.HelpText2big;
                videoPlayerLoad.helpText1small.text = Preloader.window.HelpText2small;
            }
            else if(!isHorisontal && isVertocal) 
            { 
                videoPlayerLoad.textScan.text = "2/3";
                videoPlayerLoad.helpText1big.text = Preloader.window.HelpText1big;
                videoPlayerLoad.helpText1small.text = Preloader.window.HelpText1small;
            }

            // Raycast against the location the player touched to search for planes.
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon;


            var centerPoint = new Vector3(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2, 0);

            if (Frame.Raycast(
                centerPoint.x, centerPoint.y, raycastFilter, out hit))
            {



                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if ((hit.Trackable is DetectedPlane) &&
                    Vector3.Dot(Camera.main.transform.position - hit.Pose.position,
                        hit.Pose.rotation * Vector3.up) < 0)
                {
                    Debug.Log("Hit at back of the current DetectedPlane");
                }
                else
                {

                    if (hit.Trackable is DetectedPlane)
                    {
                        DetectedPlane detectedPlane = hit.Trackable as DetectedPlane;
                        if (detectedPlane.PlaneType != DetectedPlaneType.Vertical)
                        {
                            isHorisontal = true;
                            return;
                        }
                        else 
                        {
                            isVertocal = true;
                        }
                    }



                    if (manipulator != null)
                    {
                        // manipulator.transform.position = hit.Pose.position;

                        manipulator.gameObject.transform.position = hit.Pose.position;
                        var placedObjectForward = Vector3.up;
                        manipulator.transform.rotation = Quaternion.LookRotation(placedObjectForward, hit.Pose.up);

                        // manipulator.gameObject.transform.rotation = hit.Pose.rotation;

                        // anchor.gameObject.transform.rotation = hit.Pose.rotation;

                    }
                    else
                    {



                        manipulator = Instantiate(ManipulatorPrefab);

                        anchor = hit.Trackable.CreateAnchor(hit.Pose);


                        // Make manipulator a child of the anchor.
                        manipulator.transform.SetParent(anchor.transform, false);



                        manipulator.GetComponent<Manipulator>().Select();
                    }
                }
            }


        }

        /// <summary>
        /// Function called when the manipulation is ended.
        /// </summary>
        /// <param name="gesture">The current gesture.</param>
        protected override void OnEndManipulation(TapGesture gesture)
        {
            if (gesture.WasCancelled)
            {
                return;
            }

            // If gesture is targeting an existing object we are done.
            if (gesture.TargetObject != null)
            {
                return;
            }



            if (pawnObject != null)
                return;

            isHit = true;

            // Instantiate game object at the hit pose.
            pawnObject = PawnPrefab;

            // Make game object a child of the manipulator.
            pawnObject.transform.SetParent(manipulator.transform, false);

            //// Create an anchor to allow ARCore to track the hitpoint as understanding of
            //// the physical world evolves.
            //var anchor = hit.Trackable.CreateAnchor(hit.Pose);

            //// Make manipulator a child of the anchor.
            //manipulator.transform.parent = anchor.transform;

            //// Select the placed object.
            //manipulator.GetComponent<Manipulator>().Select();

            //manipulator.GetComponent<SelectionManipulator>().SelectionVisualization.gameObject.SetActive(false);

            pawnObject.SetActive(true);

          //  videoPlayerLoad.NextUrl();


            Game.Utils.FunctionUpdater.Create(() =>
            {
                if (Preloader.window.WindowList[0].type == "video")
                {
                    bool isPrepare = false;

                    if (videoPlayerLoad.videoPlayer.videoPlayer.isPrepared)
                    {
                        videoPlayerLoad.videoPlayer.videoPlayer.Play();

                        videoPlayerLoad.top.material.mainTexture = videoPlayerLoad.videoPlayer.videoPlayer.targetTexture;
                        videoPlayerLoad.bottom.material.mainTexture = videoPlayerLoad.videoPlayer.videoPlayer.targetTexture;
                        videoPlayerLoad.center.material.mainTexture = videoPlayerLoad.videoPlayer.videoPlayer.targetTexture;

                        return true;
                    }
                    else if (!isPrepare)
                    {
                        videoPlayerLoad.videoPlayer.videoPlayer.Prepare();
                        isPrepare = true;
                    }
                }
                else
                {
                    if (Manipulator_ARTeam.textureImage != null)
                    {
                        videoPlayerLoad.top.material.mainTexture = Manipulator_ARTeam.textureImage;
                        videoPlayerLoad.bottom.material.mainTexture = Manipulator_ARTeam.textureImage;
                        videoPlayerLoad.center.material.mainTexture = Manipulator_ARTeam.textureImage;

                        float aspectRatio = (float)Manipulator_ARTeam.textureImage.width / (float)Manipulator_ARTeam.textureImage.height;

                        videoPlayerLoad.top.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                        videoPlayerLoad.top.GetComponent<AspectRatioFitter>().enabled = true;
                        videoPlayerLoad.bottom.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                        videoPlayerLoad.bottom.GetComponent<AspectRatioFitter>().enabled = true;
                        videoPlayerLoad.center.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                        videoPlayerLoad.center.GetComponent<AspectRatioFitter>().enabled = true;

                        videoPlayerLoad.top.rectTransform.anchoredPosition = new Vector2(videoPlayerLoad.top.rectTransform.anchoredPosition.x, videoPlayerLoad.top.rectTransform.sizeDelta.y);
                        videoPlayerLoad.bottom.rectTransform.anchoredPosition = new Vector2(videoPlayerLoad.bottom.rectTransform.anchoredPosition.x, -videoPlayerLoad.bottom.rectTransform.sizeDelta.y);

                   
                        return true;
                    }
                    else
                    {
                        bool isPrepareTexture = false;
                        Texture textureImage = null;

                        StartCoroutine(Manipulator_ARTeam.GetTexture(Preloader.window.WindowList[0].path, value => textureImage = value, value => isPrepareTexture = value));

                        if (isPrepareTexture)
                        {
                            videoPlayerLoad.top.material.mainTexture = textureImage;
                            videoPlayerLoad.bottom.material.mainTexture = textureImage;
                            videoPlayerLoad.center.material.mainTexture = textureImage;


                            float aspectRatio = (float)textureImage.width / (float)textureImage.height;

                            videoPlayerLoad.top.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                            videoPlayerLoad.top.GetComponent<AspectRatioFitter>().enabled = true;
                            videoPlayerLoad.bottom.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                            videoPlayerLoad.bottom.GetComponent<AspectRatioFitter>().enabled = true;
                            videoPlayerLoad.center.GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;
                            videoPlayerLoad.center.GetComponent<AspectRatioFitter>().enabled = true;

                            videoPlayerLoad.top.rectTransform.anchoredPosition = new Vector2(videoPlayerLoad.top.rectTransform.anchoredPosition.x, videoPlayerLoad.top.rectTransform.sizeDelta.y);
                            videoPlayerLoad.bottom.rectTransform.anchoredPosition = new Vector2(videoPlayerLoad.bottom.rectTransform.anchoredPosition.x, -videoPlayerLoad.bottom.rectTransform.sizeDelta.y);

                            return true;
                        }
                    }
                }

                return false;
            });

            lastRefrech = Time.time;
            // buttonNext.interactable = false;
            videoPlayerLoad.timerImage.fillAmount = 1f;
            FunctionUpdater.Create(() =>
            {
                float timer = Time.time - lastRefrech;
                if (timer > Preloader.window.TimerSec)
                {
                    if (videoPlayerLoad.GetIsPause())
                    {
                        lastRefrech = Time.time;
                        videoPlayerLoad.timerImage.fillAmount = 1;

                        return false;
                    }

                    // buttonNext.interactable = true;
                    videoPlayerLoad.NextUrl();
                    videoPlayerLoad.timerImage.fillAmount = 0f;
                    return true;
                }

                videoPlayerLoad.timerImage.fillAmount = 1 - timer / Preloader.window.TimerSec;

                return false;
            });


            videoPlayerLoad.btnNextInfo.SetActive(true);
            videoPlayerLoad.nameText.text = Preloader.window.WindowList[0].name;
            videoPlayerLoad.cityText.text = Preloader.window.WindowList[0].city;

            foreach (MeshRenderer mesh in manipulator.GetComponent<SelectionManipulator>().SelectionVisualization.GetComponentsInChildren<MeshRenderer>())
            {
                mesh.enabled = false;
            }

            //manipulator.GetComponent<ElevationManipulator>().LineRenderer.gameObject.SetActive(false);

            planeGenerator.SetActive(false);
            pointCloud.SetActive(false);
            stayImageHelp.SetActive(false);

            videoPlayerLoad.textScan.gameObject.SetActive(false);
            videoPlayerLoad.helpText1big.gameObject.SetActive(false);
            videoPlayerLoad.helpText1small.gameObject.SetActive(false);
            videoPlayerLoad.buttonNext.gameObject.SetActive(true);
            videoPlayerLoad.likeButton.enabled = true;
            videoPlayerLoad.helpNext.SetActive(true);
            FunctionTimer.Create(()=> { videoPlayerLoad.helpNext.SetActive(false); }, 10f);



        }
    }
}
