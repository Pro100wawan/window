﻿using System;
using System.Collections.Generic;


[Serializable]
public class Window  
{
    public string HelpText1big;
    public string HelpText1small;
    public string HelpText2big;
    public string HelpText2small;
    public string HelpText3big;
    public string HelpText3small;
    public string HelpNextBtn;
    public string HelpStayBtn;
    public float TimerSec;

    public List<WindowSerialized> WindowList;
    public Window()
    {
        WindowList = new List<WindowSerialized>();
    }
}


[Serializable]
public class WindowSerialized
{
    public int uniqid;
    public string name;
    public string city;
    public string country;
    public int likes;
    public string path;
    public string type;
  
}