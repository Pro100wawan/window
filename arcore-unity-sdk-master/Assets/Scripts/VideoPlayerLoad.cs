﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerLoad : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public bool isFirst;



    public void Start()
    {
        //if (Preloader.window.WindowList.Count == 1)
        //{
        //        videoPlayer.url = Preloader.window.WindowList[0].path;
        //}
        //else if (Preloader.window.WindowList.Count > 1)
        //{
        //    if (isFirst)
        //        videoPlayer.url = Preloader.window.WindowList[0].path;
        //    else
        //    {
        //        videoPlayer.url = Preloader.window.WindowList[1].path;
        //        Manipulator_ARTeam.numberVideo = 1;
        //    }
        //}

        //PrepareVideo();
         

        
    }

    public void PrepareVideo()
    {
        //Set Audio Output to AudioSource
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, GetComponent<AudioSource>());

        Game.Utils.FunctionUpdater.Create(() =>
        {
            bool isPrepare = false;

            if (videoPlayer.isPrepared)
            {
                videoPlayer.Pause();
                return true;
            }
            else if (!isPrepare)
            {
                videoPlayer.Prepare();


                isPrepare = true;
            }

            return false;
        });



        videoPlayer.Prepare();
    }
}
