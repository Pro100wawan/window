﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectToCamera : MonoBehaviour
{
    public Camera objectView;

    Plane[] planes;

    private void Update()
    {
        Vector3 viewPoint = Camera.main.WorldToViewportPoint(objectView.transform.localPosition);

        if (viewPoint.y > 1.00f)
            objectView.enabled = false;
        else
            objectView.enabled = true;
      
        print(Camera.main.WorldToViewportPoint(objectView.transform.localPosition));
    }

}
