﻿using UnityEngine;
using System.Collections;

namespace ArLib.ARSetup
{
    public class RenderTextureToProjector : MonoBehaviour
    {
        public RenderTexture _texture;
        public Projector _projector;

        public bool isRendererProject;

        // Use this for initialization
        void Start()
        {

            if (isRendererProject)
            {

                //_texture = new RenderTexture (Screen.width, Screen.height, 16);
                //_texture = new RenderTexture(256, 256, 0);
                //_texture.filterMode = FilterMode.Point;

                //gameObject.GetComponent<Camera>().targetTexture = _texture;

                //_projector.material.SetTexture("_ShadowTex", _texture);


               // gameObject.GetComponent<Camera>().targetTexture = GetComponent<CopyCameraData>().targetCamera.targetTexture;

                _projector.material.SetTexture("_ShadowTex", gameObject.GetComponent<Camera>().targetTexture);
            }
            else
            {

                Vector2 textureSize = Vector2.zero;
                float ratio = (float)Screen.height / (float)Screen.width;

                textureSize.x = Screen.width;
                textureSize.y = Screen.width * ratio;

                Debug.Log(textureSize);

                Debug.Log("Screen Size: " + Screen.width + "," + Screen.height);

                //_texture = new RenderTexture (Screen.width, Screen.height, 16);
                _texture = new RenderTexture((int)textureSize.x, (int)textureSize.y, 0);
                _texture.filterMode = FilterMode.Point;

                gameObject.GetComponent<Camera>().targetTexture = _texture;
                _projector.material.SetTexture("_ShadowTex", _texture);
            }





        }
    }
}
