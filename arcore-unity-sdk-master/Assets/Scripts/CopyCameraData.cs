﻿using UnityEngine;
using System.Collections;
namespace ArLib.ARSetup
{
    public class CopyCameraData : MonoBehaviour
    {

        public Camera targetCamera;

        private Camera _cam;
        private bool isUpdate = false;

        private void Awake()
        {
         //   DontDestroyOnLoad(gameObject);
        }

        // Use this for initialization
        IEnumerator Start()
        {
            yield return new WaitForSeconds(2.0f);
            if (!targetCamera)
                targetCamera = Camera.main;
            _cam = gameObject.GetComponent<Camera>();

            _cam.aspect = targetCamera.aspect;
            _cam.fieldOfView = targetCamera.fieldOfView;
            isUpdate = true;
        }

        private void LateUpdate()
        {
        /*    if (!isUpdate)
                return;

            _cam.transform.position = targetCamera.transform.position;
            _cam.transform.rotation = targetCamera.transform.rotation;*/
        }
    }
}
