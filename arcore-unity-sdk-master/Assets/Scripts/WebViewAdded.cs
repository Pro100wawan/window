﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelBusters.NativePlugins;

public class WebViewAdded : MonoBehaviour
{
	[SerializeField]
	private string m_url;

#if USES_WEBVIEW
	private WebView m_webview;
#endif

	private  void Start()
	{

		// Cache instances
		m_webview = FindObjectOfType<WebView>();
		
	}

	public void ShowWebView()
	{
		m_webview.Show();
	}

	public void LoadRequest()
	{
		//m_webview.LoadRequest(m_url);
		//m_webview.SetFullScreenFrame();

		Application.OpenURL(m_url);
	}

	private void OnEnable()
	{
		// Set frame
		if (m_webview != null)
			SetFrame();

		// Registering callbacks
		WebView.DidShowEvent += DidShowEvent;
		WebView.DidHideEvent += DidHideEvent;
		WebView.DidDestroyEvent += DidDestroyEvent;
		WebView.DidStartLoadEvent += DidStartLoadEvent;
		WebView.DidFinishLoadEvent += DidFinishLoadEvent;
		WebView.DidFailLoadWithErrorEvent += DidFailLoadWithErrorEvent;
		WebView.DidFinishEvaluatingJavaScriptEvent += DidFinishEvaluatingJavaScriptEvent;
		WebView.DidReceiveMessageEvent += DidReceiveMessageEvent;
	}

	private void OnDisable()
	{
		// Deregistering callbacks
		WebView.DidShowEvent -= DidShowEvent;
		WebView.DidHideEvent -= DidHideEvent;
		WebView.DidDestroyEvent -= DidDestroyEvent;
		WebView.DidStartLoadEvent -= DidStartLoadEvent;
		WebView.DidFinishLoadEvent -= DidFinishLoadEvent;
		WebView.DidFailLoadWithErrorEvent -= DidFailLoadWithErrorEvent;
		WebView.DidFinishEvaluatingJavaScriptEvent -= DidFinishEvaluatingJavaScriptEvent;
		WebView.DidReceiveMessageEvent -= DidReceiveMessageEvent;
	}

	private void SetFrame()
	{
		m_webview.Frame = new Rect(0f, Screen.height * 0.75f, Screen.width, Screen.height * 0.2f);
	}

	private void DidShowEvent(WebView _webview)
	{
		print("Displaying web view on screen.");
	}

	private void DidHideEvent(WebView _webview)
	{
		print("Dismissed web view from the screen.");
	}

	private void DidDestroyEvent(WebView _webview)
	{
		print("Released web view instance.");
	}

	private void DidStartLoadEvent(WebView _webview)
	{
		print("Started loading webpage contents.");
		print(string.Format("URL: {0}.", _webview.URL));
	}

	private void DidFinishLoadEvent(WebView _webview)
	{
		print("Finished loading webpage contents.");
	}

	private void DidFailLoadWithErrorEvent(WebView _webview, string _error)
	{
		print("Failed to load requested contents.");
		print(string.Format("Error: {0}.", _error));
	}

	private void DidFinishEvaluatingJavaScriptEvent(WebView _webview, string _result)
	{
		print("Finished evaluating JavaScript script.");
		print(string.Format("Result: {0}.", _result));
	}

	private void DidReceiveMessageEvent(WebView _webview, WebViewMessage _message)
	{
		print("Received a new message from web view.");
		print(string.Format("Host: {0}.", _message.Host));
		print(string.Format("Scheme: {0}.", _message.Scheme));
		print(string.Format("URL: {0}.", _message.URL));
		//print(string.Format("Arguments: {0}.", _message.Arguments.ToJSON()));
	}
}
