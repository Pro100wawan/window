﻿using Snappy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Networking;
using System.IO;

public class Preloader : MonoBehaviour
{
    public static Preloader Instanse;
    public static Window window;
    private void Awake()
    {
        Instanse = this;
        StartCoroutine(loadJSON());
        DontDestroyOnLoad(this);
    }


    IEnumerator loadJSON()
    {
        // UnityWebRequest www = UnityWebRequest.Get("https://bel.s3.ar.team/library/prod/window.json"); 
        UnityWebRequest www = UnityWebRequest.Get("http://lomw.ar.team/api/v2/window.php");
        www.chunkedTransfer = false;

        www.SetRequestHeader("Content-Type", "application/json");

        yield return www.SendWebRequest();


        while (!www.isDone)
        {
            Debug.Log(www.downloadProgress);
            yield return null;
        }

        if (!string.IsNullOrEmpty(www.error))
        {
            if (PlayerPrefs.HasKey("Translation"))
            {
                window = JsonUtility.FromJson<Window>(PlayerPrefs.GetString("Window"));
            }
            else
            {
                Debug.Log("Resources load translation");
                window = JsonUtility.FromJson<Window>(Resources.Load<TextAsset>("window").text);
            }
            Debug.Log(www.error);

        }
        else
        {


            if (PlayerPrefs.HasKey("Window"))
            {
                byte[] lEncodeBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(www.downloadHandler.text);
                byte[] lEncodeWWWBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(PlayerPrefs.GetString("Window"));

                if (Crc32.Compute(lEncodeBytes) != Crc32.Compute(lEncodeWWWBytes))
                    PlayerPrefs.SetString("Window", www.downloadHandler.text);
                else
                    Debug.Log("Snappy True");

                try
                {
                    window = JsonUtility.FromJson<Window>(www.downloadHandler.text);
                }
                catch
                {
                    window = JsonUtility.FromJson<Window>(Resources.Load<TextAsset>("window").text);
                }
            }
            else
            {
                try
                {
                    window = JsonUtility.FromJson<Window>(www.downloadHandler.text);
                    PlayerPrefs.SetString("Window", www.downloadHandler.text);
                }
                catch
                {
                    window = JsonUtility.FromJson<Window>(Resources.Load<TextAsset>("window").text);
                }

            }



        }


     //   StartCoroutine(DownloadVideo(window.videoFirst));
      //  StartCoroutine(DownloadVideo2(window.videoSecond));

        UnityEngine.SceneManagement.SceneManager.LoadScene("ObjectManipulation");
    }


    IEnumerator DownloadVideo(string urlVideo)
    {
        string path = Application.persistentDataPath + "/" + "1.mp4";

        UnityWebRequest www = UnityWebRequest.Get(urlVideo);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            File.WriteAllBytes(path, www.downloadHandler.data);
        }

    
    }

    IEnumerator DownloadVideo2(string urlVideo)
    {
        string path = Application.persistentDataPath + "/" + "2.mp4";

        UnityWebRequest www = UnityWebRequest.Get(urlVideo);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            File.WriteAllBytes(path, www.downloadHandler.data);
        }

    }
}
