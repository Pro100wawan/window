﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PortalManager : MonoBehaviour
{
    public GameObject Sponza;

    private Material[] SponzaMaterials;

    private Material PortalPlaneMaterial;
    private bool isFront;

    private void Start()
    {
        isFront = true;
        SponzaMaterials = Sponza.GetComponent<Renderer>().sharedMaterials;
        PortalPlaneMaterial = GetComponent<Renderer>().sharedMaterial;

        foreach (var mat in SponzaMaterials)
        {
            mat.SetInt("_StencilComp", (int)CompareFunction.Equal);
        }

        PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Off);

    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 worldPos = Camera.main.transform.position + Camera.main.transform.forward * Camera.main.nearClipPlane;

        Vector3 camPositionInPortalSpace = transform.InverseTransformPoint(worldPos);

        if (camPositionInPortalSpace.y > 0.0f)
            isFront = true;
        else
            isFront = false;

        //Debug.Log(isFront);

    }

    private void OnTriggerStay(Collider other)
    {
      //  Vector3 camPositionInPortalSpace = transform.InverseTransformPoint(Camera.main.transform.position);

        Vector3 worldPos = Camera.main.transform.position + Camera.main.transform.forward * Camera.main.nearClipPlane;

        Vector3 camPositionInPortalSpace = transform.InverseTransformPoint(worldPos); 

       Debug.Log(System.Math.Round(camPositionInPortalSpace.y,2));

        float positionRound = (float)System.Math.Round(camPositionInPortalSpace.y, 2);

        if (isFront)
        {
            if (positionRound <= 0.0f)
            {
                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.NotEqual);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Front);

            }
            else if (positionRound < 0.5f && positionRound > 0.05f)
            {
                // Disable Stencil test

                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.LessEqual);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Off);
            }
            else if (positionRound < 0.05f)
            {
                // Disable Stencil test

                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.Always);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Off);
            }
            else
            {
                // Enable stencil

                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.Equal);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Back);
            }
        }
        else
        {
            if (positionRound < 0.0f)
            {
                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.NotEqual);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Front);

            }
            else if (positionRound < -0.5f && positionRound > -0.05f)
            {
                // Disable Stencil test

                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.LessEqual);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Off);
            }
            else if (positionRound < -0.05f)
            {
                // Disable Stencil test

                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.Always);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Off);
            }
            else
            {
                // Enable stencil

                foreach (var mat in SponzaMaterials)
                {
                    mat.SetInt("_StencilComp", (int)CompareFunction.Equal);
                }

                PortalPlaneMaterial.SetInt("_CullMode", (int)CullMode.Back);
            }
        }


    }
}
